import game_kit as gk


class GkTestGame(gk.Game):
    def __init__(self, container):
        super().__init__(container)

        self.title = "Gk Test"
        self.window_size = (1280, 720)