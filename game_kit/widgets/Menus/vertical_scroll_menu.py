from .menu import *
from ...controllers.device_controllers import *
from ...constants import *
from ...utils import *


class VerticalScrollMenu(Menu):

    def __init__(self, structure, container):
        super().__init__(structure, container)

        # Propriété pour le scroll
        self.offset_y = 0
        self.curItemY = 0
        self.curItemHeight = 0
        self.viewHeight = 0
        self.itemAverageHeight = 0
        self.itemAverageVisible = 0

        self.loop_timer = Timer({
            'delay': 200,
            'loop': False,
            'state': [
                True,
                False
            ]
        }, paused=True)

    def update(self, dt):

        self.loop_timer.update(dt)
        # On calcule un offset pour que l'item se trouve bien dans l'écran
        if self.curItemY < (1/4)*self.viewHeight:
            self.offset_y += 100*dt + ((1/4)*self.viewHeight - self.curItemY) / 10
        # On ajoute la hauteur de l'item pour avoir son dessous à la limite
        if self.curItemY + self.curItemHeight > (1 - 1/4)*self.viewHeight:
            self.offset_y -= 100*dt + (self.curItemY + self.curItemHeight - (1 - 1/4)*self.viewHeight) / 10

    # On réécris la méthode de dessin car le menu n'a pas de vue propre
    def draw(self, surface, offset=(0, 0)):
        # On récupère la hauteur de la surface sur laquelle on dessine
        self.viewHeight = surface.get_height()
        # On calcule la hauteur du menu
        self.rect.h = 0
        for item in self['items']:
            # On donne en argument les dimensions du menu pour les calculs utilisant des coefficients
            item.compute_size(self.rect)
            self.rect.h += item.rect.h + self['interval']

        self.itemAverageHeight = self.rect.h / len(self['items'])

        self.itemAverageVisible = self.viewHeight / self.itemAverageHeight + 2

        # On initialise l'offset du menu
        if self.first_draw:
            self.offset_y = self.viewHeight//4

        # On ajoute à la hauteur actuelle l'offset calculé pour que l'item sélectionner soit bien dans l'écran
        # print(self.rect.y)
        self.rect.y += self.offset_y

        vertical_offset = 0
        for index, item in enumerate(self['items']):
            # transmission des arguments
            item.index = index
            # Récupération de l'état de l'item
            is_selected = True if self.cur_item_id == index else False
            # si l'état est différent on met à jour
            if item.selected != is_selected:
                item.selected = is_selected
                # On force l'item à se redessiner
                item.clear()
            # Calcul de l'écart de distance de l'item par rapport à son origine
            offset = self.rect.x, self.rect.y + vertical_offset
            # Si l'item est sélectionner
            if item.selected:
                # On récupère sa position et sa hauteur
                self.curItemY = offset[1]
                self.curItemHeight = item.rect.h
            # On vérifie si l'item est visible OU si le timer de boucle tourne
            if self.is_visible(index) or not self.loop_timer.paused:
                # On dessine l'item sur la surface
                item.draw(surface, offset)

            # On ajoute à l'offset un interval ainsi que la hauteur de l'item
            vertical_offset += self['interval'] + item.rect.h

        # Ce n'est plus le premier dessin
        self.first_draw = False

    # On regarde si l'item est potentiellement visible
    def is_visible(self, index):
        if self.cur_item_id - self.itemAverageVisible < index < self.cur_item_id + self.itemAverageVisible:
            return True
        return False

    def select_next_item(self):
        index = self.cur_item_id
        index += 1
        if index >= len(self['items']):
            if self['loop']:
                index = 0
                self.loop_timer['delay'] = 20 * len(self['items'])
                self.loop_timer.reset(paused=False)
            else:
                index -= 1
        self.cur_item_id = index

    def select_previous_item(self):
        index = self.cur_item_id
        index -= 1
        if index < 0:
            if self['loop']:
                index = len(self['items']) - 1
                self.loop_timer['delay'] = 20 * len(self['items'])
                self.loop_timer.reset(paused=False)
            else:
                index += 1
        self.cur_item_id = index

    def handle(self, event):
        #On appelle la méthode originale
        super().handle(event)

        if isinstance(event, ArrowDownEvent):
            if event.direction == UP:
                self.select_previous_item()
            elif event.direction == DOWN:
                self.select_next_item()


"""
    Example :

    VerticalMenu({
        'name': 'VerticalMenu',
        'size': SIZE_ABSOLUTE|SIZE_COEF,
        'width': 1,
        'height': 1,
        'position': POSITION_ABSOLUTE|POSITION_COEF,
        'x': 0,
        'y': 0,
        'interval': 25,
        'loop': True|False,
        'items': []
    }, ev_manager)
"""