from .menu import *
from ...controllers.device_controllers import *
from ...constants import *


class HorizontalMenu(Menu):

    def __init__(self, structure, container):
        super().__init__(structure, container)

    # On réécris la méthode de dessin car le menu n'a pas de vue propre
    def draw(self, surface, offset=(0, 0)):
        # On calcule la hauteur du menu pour pouvoir le centrer
        self.rect.h = 0
        for item in self['items']:
            # On donne en argument les dimensions du menu pour les calculs utilisant des coefficients
            item.compute_size(self.rect)
            self.rect.w += item.rect.w + self['interval']

        # On centre le menu sur l'axe horizontal
        self.rect.x += surface.get_width()/2 - self.rect.w/2

        horizontal_offset = 0
        for index, item in enumerate(self['items']):
            # transmission des arguments
            item.index = index
            # Récupération de l'état de l'item
            is_selected = True if self.cur_item_id == index else False
            # si l'état est différent on met à jour
            if item.selected != is_selected:
                item.selected = is_selected
                # On force l'item à se redessiner
                item.clear()
            # Calcul de l'écart de distance de l'item par rapport à son origine
            offset = self.rect.x + horizontal_offset, self.rect.y
            # On dessine l'item sur la surface
            item.draw(surface, offset)
            # On ajoute à l'offset un interval ainsi que la hauteur de l'item
            horizontal_offset += self['interval'] + item.rect.w

        # Ce n'est plus le premier dessin
        self.first_draw = False

    def select_next_item(self):
        index = self.cur_item_id
        index += 1
        if index >= len(self['items']):
            if self['loop']:
                index = 0
            else:
                index -= 1
        self.cur_item_id = index

    def select_previous_item(self):
        index = self.cur_item_id
        index -= 1
        if index < 0:
            if self['loop']:
                index = len(self['items']) - 1
            else:
                index += 1
        self.cur_item_id = index

    def handle(self, event):
        #On appelle la méthode originale
        super().handle(event)

        if isinstance(event, ArrowDownEvent):
            if event.direction == LEFT:
                self.select_previous_item()
            elif event.direction == RIGHT:
                self.select_next_item()


"""
    Example :

    self.WelcomeMenu = HorizontalMenu({
            'name': 'Welcome Menu',
            'size': SIZE_ABSOLUTE,
            'width': 0,
            'height': 500,
            'position': POSITION_COEF,
            'x': 0,
            'y': 1/4,
            'interval': 25,
            'loop': False,
            'items': [
                MenuItem({
                    'name': 'item 1',
                    'size': SIZE_ABSOLUTE,
                    'width': 200,
                    'height': 500,
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'callback': CacaoEvent,
                    'children': [
                        Label({
                            'name': 'Item 1',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 1',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]

                }, self.ev_manager),
                MenuItem({
                    'name': 'item 2',
                    'size': SIZE_ABSOLUTE,
                    'width': 200,
                    'height': 500,
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'callback': CacaoEvent,
                    'children': [
                        Label({
                            'name': 'Item 2',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 2',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]

                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': 200,
                    'height': 500,
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager)
            ]
        }, self.ev_manager)
"""