from ..widget import *
from ...controllers.device_controllers import *

class Menu(Widget):

    def __init__(self, structure, container):
        super().__init__(structure, container)

        self.cur_item_id = self['curItem'] if 'curItem' in self else 0

    def update(self, dt):
        # On transmet en premier lieu l'ordre d'update aux items
        for item in self['items']:
            item.update(dt)

    def handle(self, event):

        # Si on détecte un clique on vérifie tous les items
        if isinstance(event, MouseLeftDownEvent):
            for index, item in enumerate(self['items']):
                if item.is_click(event.pos):
                    # Si un est bien sélectionner on le sélectionne
                    self.cur_item_id = index