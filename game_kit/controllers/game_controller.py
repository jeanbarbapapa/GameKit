from .device_controllers import *
from ..scene import *
from ..container import *
from ..entities import *


class GameController:

    def __init__(self):

        # Création du container qui contient tous les services essentielles
        self.container = Container(self)
        # Création d'un accès directe à l'EventManager
        self.ev_manager = self.container.ev_manager
        self.ev_manager.register(self)

        # Création des controlleurs "matériels"
        self.keyboard_controller = KeyboardController(self.container)
        self.mouse_controller = MouseController(self.container)

        # Création des propriétés de bases
        self.entities = {}
        self.clock = pygame.time.Clock()
        self.running = False
        self.scene = None
        # Permet de stocker la classe de la scène à loader
        self.scene_class = Scene

    def run(self):

        # On initialise pygame
        pygame.init()
        # Configuration de base pour la répétition des touches enfoncées
        pygame.key.set_repeat(75, 75)

        if not 'game' in self.entities:
            self.entities['game'] = Game(self.container)

        self.view_manager = ViewManager(self.container)
        # On charge la scène donné en argument
        self.load_scene(self.scene_class)

        self.running = True

        while self.running:
            self.update()

    def update(self):
        # Temps entre 2 frames en ms
        self.clock.tick()
        dt = self.clock.get_rawtime()/1000.0
        # On transmet l'update au controller
        self.mouse_controller.update(dt)
        self.keyboard_controller.update(dt)

        # On récupère les évènements
        for event in pygame.event.get():
            # On transmet au KeyboardController les évènements de pygame
            self.keyboard_controller.handle_pygame_event(event)
            # On transmet au MouseController les évènements de pygame
            self.mouse_controller.handle_pygame_event(event)

            # Le GameController traite lui aussi les évènements
            if event.type == pygame.QUIT:
                self.running = False

        for entity in self.entities.values():
            entity.update(dt)

        # Transmet l'update
        self.scene.update(dt)

        # On dessine
        self.view_manager.draw_scene(self.scene)
        self.view_manager.draw_fps(str(round(self.clock.get_fps())))

        # Une fois que tout est dessiné on met à jour l'écran
        self.view_manager.flip()

    def add_entity(self, tag, entity_class):
        self.entities[tag] = entity_class(self.container)

    def delete_entity(self, tag):
        if tag in self.entities:
            del self.entities[tag]

    def load_scene(self, scene_class):
        # On charge la scène en lui transmettant le container en argument
        self.scene = scene_class(self.container)

    def preload_scene(self, scene_class):
        self.scene_class = scene_class

    def handle(self, event):
        pass
