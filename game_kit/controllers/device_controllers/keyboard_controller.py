import pygame
import platform
from ...event import *
from ...constants import *


class KeyboardController:

    def __init__(self, container):
        self.container = container
        self.ev_manager = self.container.ev_manager

    # Permet de mettre à jour le controller en transmettant le dt
    def update(self, dt):
        pass

    # Permet de réagir aux évènement émis par le jeu
    def handle(self, event):
        pass

    # Permet de réagir aux évènements émis par pygame qui sert ici d'interface avec le matériel
    def handle_pygame_event(self, event):

        shortcut_modifier = pygame.KMOD_LCTRL, pygame.KMOD_RCTRL # On met CTRL de base
        # Si on est sous Mac
        if platform.system() == 'Darwin':
            shortcut_modifier = pygame.KMOD_LMETA, pygame.KMOD_RMETA


        # Common Events
        if event.type == pygame.KEYDOWN:
            self.ev_manager.post(KeyDownEvent(self, event))
        if event.type == pygame.KEYUP:
            self.ev_manager.post(KeyUpEvent(self, event))

        # Return
        if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
            self.ev_manager.post(ReturnDownEvent(self))

        # Backspace
        if event.type == pygame.KEYDOWN and event.key == pygame.K_BACKSPACE:
            self.ev_manager.post(BackspaceDownEvent(self))

        # Delete
        if event.type == pygame.KEYDOWN and event.key == pygame.K_DELETE:
            self.ev_manager.post(DeleteDownEvent(self))

        #On récupère le modifier actuel pour tester si on est en présence de control clavier
        modifier = pygame.key.get_mods()
        if modifier in shortcut_modifier:
            # Clipboard Shortcut
            if event.type == pygame.KEYDOWN and event.key == pygame.K_c:
                self.ev_manager.post(CopyEvent(self))
            if event.type == pygame.KEYDOWN and event.key == pygame.K_x:
                self.ev_manager.post(CutEvent(self))
            if event.type == pygame.KEYDOWN and event.key == pygame.K_v:
                self.ev_manager.post(PasteEvent(self))

            # Text Edit Shortcut
            if event.type == pygame.KEYDOWN and event.key == pygame.K_a:
                self.ev_manager.post(SelectAllEvent(self))

        if event.type == pygame.KEYDOWN and event.key == pygame.K_HOME:
            self.ev_manager.post(FirstDownEvent(self))
        if event.type == pygame.KEYDOWN and event.key == pygame.K_END:
            self.ev_manager.post(EndDownEvent(self))

        # Directional Arrows
        if event.type == pygame.KEYDOWN and event.key == pygame.K_UP:
            self.ev_manager.post(ArrowDownEvent(self, UP))
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
            self.ev_manager.post(ArrowDownEvent(self, DOWN))
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
            self.ev_manager.post(ArrowDownEvent(self, LEFT))
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
            self.ev_manager.post(ArrowDownEvent(self, RIGHT))

        pass


# EVENTS
# On retrouve ici tous les évènements associé au keyboard

class KeyDownEvent(Event):
    def __init__(self, sender, pygame_event):
        super().__init__(sender)
        self.name = "Key Down Event"
        self.key_code = pygame_event.key
        self.modifiers = pygame.key.get_mods()
        self.pygame_event = pygame_event

        # Liste des code que l'on ne prend pas
        # 8, 127 : Backspace | 27: Escape | 13: Enter | 3: NumpadEnter | 9:Tab
        self.blacklist = [8, 127, 27, 13, 3, 9]

    def is_shortcut(self):
        # On met CTRL de base
        shortcut_modifier_l = pygame.KMOD_LCTRL
        shortcut_modifier_r = pygame.KMOD_RCTRL
        # Si on est sous Mac
        if platform.system() == 'Darwin':
            shortcut_modifier_l = pygame.KMOD_LMETA # On met CMD
            shortcut_modifier_r = pygame.KMOD_RMETA

        if self.modifiers & shortcut_modifier_l or self.modifiers & shortcut_modifier_r:
            return True
        return False

    def get_char(self):

        char = self.pygame_event.unicode

        if len(char) > 0:

            charCode = ord(char)
            if charCode > 250 or charCode in self.blacklist:
                return ''

            if self.modifiers & pygame.KMOD_RSHIFT or self.modifiers & pygame.KMOD_LSHIFT:
                char = char.upper()

        return char


class KeyUpEvent(Event):
    def __init__(self, sender, pygame_event):
        super().__init__(sender)
        self.name = "Key Up Event"
        self.key_code = pygame_event.key
        self.modifiers = pygame.key.get_mods()
        self.pygame_event = pygame_event


class ArrowDownEvent(Event):
    def __init__(self, sender, direction):
        super().__init__(sender)
        self.name = "Arrow Down Event"
        self.modifiers = pygame.key.get_mods()
        self.direction = direction


class ArrowUpEvent(Event):
    def __init__(self, sender, direction):
        super().__init__(sender)
        self.name = "Arrow Up Event"
        self.modifiers = pygame.key.get_mods()
        self.direction = direction


class ReturnDownEvent(Event):
    def __init__(self, sender):
        super().__init__(sender)
        self.name = "Return Down Event"


class ReturnUpEvent(Event):
    def __init__(self, sender):
        super().__init__(sender)
        self.modifiers = pygame.key.get_mods()
        self.name = "Return Up Event"

class BackspaceDownEvent(Event):
    def __init__(self, sender):
        super().__init__(sender)
        self.modifiers = pygame.key.get_mods()
        self.name = "Backspace Down Event"


class BackspaceUpEvent(Event):
    def __init__(self, sender):
        super().__init__(sender)
        self.modifiers = pygame.key.get_mods()
        self.name = "Backspace Up Event"


class DeleteDownEvent(Event):
    def __init__(self, sender):
        super().__init__(sender)
        self.modifiers = pygame.key.get_mods()
        self.name = "Delete Down Event"


class DeleteUpEvent(Event):
    def __init__(self, sender):
        super().__init__(sender)
        self.modifiers = pygame.key.get_mods()
        self.name = "Delete Up Event"

class CopyEvent(Event):
    def __init__(self, sender):
        super().__init__(sender)
        self.modifiers = pygame.key.get_mods()
        self.name = "Copy Event"


class CutEvent(Event):
    def __init__(self, sender):
        super().__init__(sender)
        self.modifiers = pygame.key.get_mods()
        self.name = "Cut Event"


class PasteEvent(Event):
    def __init__(self, sender):
        super().__init__(sender)
        self.modifiers = pygame.key.get_mods()
        self.name = "Paste Event"

class SelectAllEvent(Event):
    def __init__(self, sender):
        super().__init__(sender)
        self.modifiers = pygame.key.get_mods()
        self.name = "Select All Event"

class FirstDownEvent(Event):
    def __init__(self, sender):
        super().__init__(sender)
        self.modifiers = pygame.key.get_mods()
        self.name = "First Down Event"

class EndDownEvent(Event):
    def __init__(self, sender):
        super().__init__(sender)
        self.modifiers = pygame.key.get_mods()
        self.name = "End Down Event"


