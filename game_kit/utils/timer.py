from .util import *

class Timer(Util):

    def __init__(self, structure, paused=False):
        super().__init__(structure)

        self.paused = paused
        self.timer = 0.0
        self.state_id = 0

        self.reset(paused=paused)


    def update(self, dt):

        # Si le timer est en pause on s'arrete là
        if self.paused:
            return


        self.timer -= dt*1000.0

        # Le timer s'est fini on passe à l'état suivant
        if self.timer < 0.0:
            if self.get('loop', True):
                # On reset le timer
                self.timer = float(self['delay'])
            else:
                self.pause()

            new_state_id = self.state_id + 1
            if new_state_id >= len(self['state']):
                # Si il n'y a pas d'élement après on revient au premier élement
                new_state_id = 0
            self.state_id = new_state_id

    def pause(self):
        self.paused = True

    def play(self):
        self.paused = False

    def reset(self, paused=False):
        self.timer = float(self['delay'])
        self.state_id = self.get('default_state', 0)
        self.paused = paused

    def get_state(self):
        return self['state'][self.state_id]


"""
Timer({
    'delay': 200,
    'loop': True,
    'state': [
        False,
        True
    ]
}, paused=True)
"""