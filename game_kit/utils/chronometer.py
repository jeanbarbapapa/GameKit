from .util import *

class Chronometer(Util):

    def __init__(self, structure, paused=True):
        super().__init__(structure)

        self.paused = paused
        self.time = 0.0

        self.reset(paused=paused)


    def update(self, dt):

        # Si le timer est en pause on s'arrete là
        if self.paused:
            return

        self.time += dt

    def pause(self):
        self.paused = True

    def play(self):
        self.paused = False

    def reset(self, paused=False):
        self.time = 0.0
        self.paused = paused


"""
Timer({}, paused=True)
"""